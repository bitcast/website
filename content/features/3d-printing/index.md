---
title: 3D Printing
---
{{< figure src="static/ring.png" alt="Custom ring" caption="Custom printed ring" width="300px" class="fig-right fig-img" >}}

From sculptured miniatures and props to mechanical parts, Bitcast has a team of
dedicated 3D printing experts ready to help you. Choose from our catalogue of
ready-to-print objects, bring your own or work with our 3D specialists to
develop the perfect part. We are capable of printing parts from UV photosensitive
resin, using LCD SLA printers for that high-quality finish, down to 47um DPI.

SLA printing uses UV light to cure resin in very thin layers. This leaves a
superior finish to traditional extruded prints without any loss in strength.
The finish can be painted on or left bare with minimal clean-up. Once your part
is printed, our artists are eager to help achieve your vision for the object
through painting and styling parts as desired.

[Contact Us](/contact/) to talk to one of our graphic artists about your
project.

{{< carousel "printed-items" >}}
{{< carousel-item src="static/dnd_group.png" alt="Custom DnD characters" caption="Custom printed Dungeons and Dragons characters" class="active" >}}
{{< carousel-item src="static/rams_head.png" alt="Ram's Head sculpture" caption="A beautiful 3D printed Ram's Head sculpture" >}}
{{< carousel-item src="static/arm_raised_low.png" alt="Raised arm sculpture" caption="A 3D printed man with his arm raised painted bronze" >}}
{{< carousel-item src="static/dnd_painted.png" alt="Custom painted DnD characters" caption="Custom printed and painted Dungeons and Dragons characters" >}}
{{< carousel-item src="static/dnd_2.png" alt="Custom zombie characters" caption="Zombie Dungeons and Dragons characters straight off the printer" >}}
{{< carousel-item src="static/door_1.png" alt="Minature realistic door" caption="Miniature hyper-realistic door entrance scene" >}}
{{< /carousel >}}
